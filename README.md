# Table

Table is a WIP digital drawing application born from my frustration from
existing drawing applications. My main points of focus are UI, high
performance, and cross-platform compatibility.

### Table with the Light Theme
![light-theme](res/img/light-theme.png)

### Table with the Dark Theme
![dark-theme](res/img/dark-theme.png)
