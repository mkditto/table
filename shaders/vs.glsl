// #version 330
//
// layout (location = 0) in vec3 position;
// layout (location = 1) in vec3 color;
//
// out vec3 triangleColor;
//
// uniform int width;
// uniform int height;
//
// void main() {
//     mat4 orthoMatrix = mat4(
//         vec4( 2.0 / width,     0.0     ,     0.0     ,     0.0    ),
//         vec4(     0.0    , 2.0 / height,     0.0     ,     0.0    ),
//         vec4(     0.0    ,     0.0     ,    -1.0     ,     0.0    ),
//         vec4(    -1.0    ,    -1.0     ,     0.0     ,     1.0    )
//     );
//
//     gl_Position = orthoMatrix * vec4(position, 1.0f);
//     triangleColor = color;
// }
#version 330

layout (location = 0) in vec2 position;
void main() {
    gl_Position = vec4(position, 0.0, 1.0);
}
