// #version 330
//
// in vec3 triangleColor;
// out vec4 color;
//
// void main() {
//     color = vec4(triangleColor, 1.0f);
//     // color = vec4(31 / 255.0f, 31 / 255.0f, 31 / 255.0f, 1.0f);
// }
#version 330

out vec4 color;

void main() {
    color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}
