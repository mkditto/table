use super::gl;
use super::gl::types::*;

use super::style::Variant;

use gtk::Inhibit;
use gtk::{GLArea, GLAreaExt,
          Widget, WidgetExt};

use gdk::{GLContext, GLContextExt};

use std::sync::{Arc, Mutex};


#[derive(Clone, Debug)]
pub struct Canvas {
    gl: GLArea,
    pub bg: (f32, f32, f32),
    size: (u32, u32),
    pub vbo: Arc<Mutex<GLuint>>,
    pub vao: Arc<Mutex<GLuint>>,
    pub ebo: Arc<Mutex<GLuint>>,
    pub prog: Arc<Mutex<GLuint>>,
}

impl Canvas {
    pub fn new() -> Canvas {
        let gl = GLArea::new();
        gl.set_hexpand(true);
        gl.set_vexpand(true);

        Canvas {
            gl: gl,
            bg: (32.0 / 255.0, 32.0 / 255.0, 32.0 / 255.0),
            // bg: (144.0 / 255.0, 144.0 / 255.0, 144.0 / 255.0),
            size: (0, 0),
            vbo: Arc::new(Mutex::new(0)),
            vao: Arc::new(Mutex::new(0)),
            ebo: Arc::new(Mutex::new(0)),
            prog: Arc::new(Mutex::new(0)),
        }
    }

    pub fn get_gtk_gl(&self) -> &GLArea {
        &self.gl
    }


    pub fn set_realize_fn<F: Fn(&GLArea) + 'static>(&self, f: F) {
        self.gl.connect_realize(f);
    }

    pub fn set_resize_fn<F: Fn(&GLArea, i32, i32) + 'static>(&self, f: F) {
        self.gl.connect_resize(f);
    }

    pub fn set_render_fn<F: Fn(&GLArea, &GLContext) -> Inhibit + 'static>(&self, f: F) {
        self.gl.connect_render(f);
    }

    pub fn update_size(&mut self, w: u32, h: u32) {
        self.size = (w, h);
    }

    pub fn update_bg(&mut self, v: Variant) {
        let (r, g, b) = match v {
            Variant::Darker  => (32.0, 32.0, 32.0),
            Variant::Lighter => (144.0, 144.0, 144.0),
            _                => (255.0, 0.0, 0.0),
        };

        self.bg = (r /  255.0, g / 255.0, b / 255.0);
    }
}
