trait Dialog {}
trait Window {}

trait Canvas {}
trait Renderable {}

trait Brush {}
trait Layer {}
trait Filter {}
