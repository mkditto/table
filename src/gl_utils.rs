extern crate glutin;
extern crate colored;

use std::ptr;
use std::str;
use std::ffi::CString;
use std::os::raw::c_void;

use super::gl;
use gl::types::*;

use self::colored::*;
use self::glutin::{Api, GlContext, GlRequest};


pub struct GlUtils;

impl GlUtils {
    pub fn compile_shader(src: &str, ty: GLenum) -> GLuint {
        unsafe {
            let shader = gl::CreateShader(ty);
            let c_str = CString::new(src.as_bytes()).unwrap();

            gl::ShaderSource(shader, 1, &c_str.as_ptr(), ptr::null());
            gl::CompileShader(shader);

            let mut status = gl::FALSE as GLint;
            gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);

            if status != (gl::TRUE as GLint) {
                let mut len = 0;
                gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut len);

                let mut buf = Vec::with_capacity(len as usize);
                gl::GetShaderInfoLog(
                    shader,
                    len,
                    ptr::null_mut(),
                    buf.as_mut_ptr() as *mut GLchar,
                );

                panic!(
                    "{}",
                    str::from_utf8(&buf).ok().expect(
                        "ShaderLogInfo not valid UTF-8",
                    )
                );
            }
            shader
        }
    }

    pub fn link_program(vs: GLuint, fs: GLuint) -> GLuint {
        unsafe {
            let program = gl::CreateProgram();
            gl::AttachShader(program, vs);
            gl::AttachShader(program, fs);
            gl::LinkProgram(program);

            let mut status = gl::FALSE as GLint;
            gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);

            if status != (gl::TRUE as GLint) {
                let mut len: GLint = 0;
                gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut len);

                let mut buf = Vec::with_capacity(len as usize);
                gl::GetProgramInfoLog(
                    program,
                    len,
                    ptr::null_mut(),
                    buf.as_mut_ptr() as *mut GLchar,
                );

                panic!(
                    "{}",
                    str::from_utf8(&buf).ok().expect(
                        "ProgramLogInfo not valid UTF-8",
                    )
                );
            }
            program
        }
    }

    pub fn load_gl() {
        let context = glutin::HeadlessRendererBuilder::new(0, 0)
            .with_gl(GlRequest::Specific(Api::OpenGl, (3, 0)))
            .build_strict()
            .unwrap();

        gl::load_with(|s| {
            unsafe {
                let ptr = context.get_proc_address(s) as *const c_void;

                #[cfg(debug_assertions)]
                println!(
                    "{func:>width$} => {result}",
                    func = s,
                    width = 40,
                    result = match ptr.as_ref() {
                        Some(_) => format!("{:?}", ptr).green(),
                        None    => "FAILED".red(),
                    },
                );
                ptr
            }
        });
    }
}
