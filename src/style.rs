use std::fmt;
use std::fs::File;
use std::io::prelude::*;

use gtk::{CssProvider, CssProviderExt,
          StyleContext};

use gdk::Screen;


pub enum Variant {
    Lighter,
    Light,
    Dark,
    Darker,
}

impl fmt::Display for Variant {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}",
            match *self {
                Variant::Lighter => "lighter",
                Variant::Light => "light",
                Variant::Dark => "dark",
                Variant::Darker => "darker",
            }
        )
    }
}

pub struct Theme;

impl Theme {
    #[cfg(not(feature = "hc"))]
    pub fn update_theme(_variant: &Variant) {
        println!("Stylesheet updating...");
        let provider = CssProvider::new();
        let screen = Screen::get_default().unwrap();

        let mut style = String::new();
        let mut base = File::open("res/style/base/flat.css").unwrap();
        let mut color = File::open(format!("res/style/variant/{}.css", _variant)).unwrap();

        #[cfg(target_os = "windows")]
        let mut system = File::open("res/style/platform/win32/style.css").unwrap();
        #[cfg(any(target_os = "linux", target_os = "freebsd", target_os="dragonfly", target_os="openbsd"))]
        let mut system = File::open("res/style/platform/linux/style.css").unwrap();

        color.read_to_string(&mut style).unwrap();

        base.read_to_string(&mut style).unwrap();
        system.read_to_string(&mut style).unwrap();

        provider.load_from_data(style.as_bytes()).unwrap();
        StyleContext::add_provider_for_screen(
            &screen,
            &provider,
            super::gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
    }

    #[cfg(feature = "hc")]
    pub fn update_theme() {
        let provider = CssProvider::new();
        let screen = Screen::get_default().unwrap();

        let mut style = String::new();
        let base = include_str!("../res/style/base/flat.css");

        #[cfg(target_os = "windows")]
        let system = include_str!("../res/style/platform/win32/style.css");
        #[cfg(any(target_os = "linux", target_os = "freebsd", target_os="dragonfly", target_os="openbsd"))]
        let system = include_str!("../res/style/platform/linux/style.css");

        style.push_str(&base);
        style.push_str(&system);

        provider.load_from_data(style.as_bytes()).unwrap();
        StyleContext::add_provider_for_screen(
            &screen,
            &provider,
            super::gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
    }
}
