use gdk::{GLContext, GLContextExt,
          WindowExt as GdkWindowExt};

use gtk::{Box as GtkBox,
          Builder, BuilderExt,
          ContainerExt,
          GLArea, GLAreaExt,
          Inhibit,
          IsA,
          MenuItem, MenuItemExt,
          Object,
          ToolButton, ToolButtonExt,
          Widget, WidgetExt,
          Window, WindowExt};

use std::mem;
use std::path::Path;
use std::os::raw::c_void;
use std::time::{SystemTime};
use std::sync::{Arc, Mutex};

use super::gl;
use super::GlUtils;
use super::canvas::Canvas;
use super::style::{Theme, Variant};

#[derive(Clone, Debug)]
pub struct MainWindow {
    pub builder: Builder,
    pub window: Window,
    pub canvas:Canvas,
}

impl MainWindow {
    pub fn init() -> MainWindow {
        let builder = Builder::new();
        builder.add_from_string(include_str!("../res/layout/window.glade")).unwrap();

        let window = builder.get_object::<Window>("main-window").unwrap();
        let mut canvas = Canvas::new();

        MainWindow {
            builder: builder,
            window: window,
            canvas: canvas,
        }
    }

    pub fn create(&self) {
        let window = &self.window;
        let canvas = &self.canvas;
        let reload_thm = self.builder.get_object::<MenuItem>("reload-theme-item").unwrap();
        let draw_area = self.builder.get_object::<GtkBox>("gl-container").unwrap();
        let start = SystemTime::now();

        {
            let (vbo, ebo, vao, prog) =
                (canvas.vbo.clone(), canvas.ebo.clone(), canvas.vao.clone(), canvas.prog.clone());
            canvas.set_realize_fn(move |gl_area| {
                    gl_area.get_context().unwrap().make_current();
                    GlUtils::load_gl();

                    #[cfg(debug_assertions)]
                    match gl_area.get_error() {
                        Some(e) =>  println!("Gl Error : {}", e),
                        None    =>  (),
                    }

                    unsafe {
                        let verts: [f32; 8] = [
                             0.95,  0.95,
                            -0.95,  0.95,
                            -0.95, -0.95,
                             0.95, -0.95
                        ];
                        let indices: [u16; 6] = [
                            0, 1, 2,
                            2, 3, 0
                        ];

                        let vert_shader_source = include_str!("../shaders/vs.glsl");
                        let frag_shader_source = include_str!("../shaders/fs.glsl");

                        let mut vbo = vbo.lock().unwrap();
                        let mut ebo = ebo.lock().unwrap();
                        let mut vao = vao.lock().unwrap();
                        let mut prog = prog.lock().unwrap();

                        let vert_shader = GlUtils::compile_shader(
                            vert_shader_source,
                            gl::VERTEX_SHADER,
                        );
                        let frag_shader = GlUtils::compile_shader(
                            frag_shader_source,
                            gl::FRAGMENT_SHADER,
                        );
                        *prog = GlUtils::link_program(vert_shader, frag_shader);

                        gl::GenBuffers(1, &mut *vbo);
                        gl::GenBuffers(1, &mut *ebo);
                        gl::GenVertexArrays(1, &mut *vao);

                        gl::BindBuffer(gl::ARRAY_BUFFER, *vbo);
                        gl::BufferData(
                            gl::ARRAY_BUFFER,
                            8 * 4 /*8x f32*/,
                            mem::transmute(&verts),
                            gl::STATIC_DRAW,
                        );
                        gl::BindBuffer(gl::ARRAY_BUFFER, 0);

                        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, *ebo);
                        gl::BufferData(
                            gl::ELEMENT_ARRAY_BUFFER,
                            6 * 2 /*6x i16*/,
                            mem::transmute(&indices),
                            gl::STATIC_DRAW,
                        );
                        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
                    }
                });
        }

        {
            let (vbo, ebo, vao, prog) =
                (canvas.vbo.clone(), canvas.ebo.clone(), canvas.vao.clone(), canvas.prog.clone());
            let (r, g, b) = canvas.bg.clone();
            canvas.set_render_fn(move |gl_area, _context| {
                    unsafe {
                        let now = SystemTime::now();
                        let dur = now.duration_since(start).expect("RIP");
                        let millis = dur.as_secs() * 1_000 + (dur.subsec_nanos() as u64) / 1_000_000;

                        let _t = ((millis % 2000) as f32) / 1000.0;

                        // gl::ClearColor(_t / 2.0, 1.0 - (_t / 2.0), 1.0, 1.0);

                        gl::ClearColor(r, g, b, 1.0);
                        gl::Clear(gl::COLOR_BUFFER_BIT);

                        let vbo = vbo.lock().unwrap();
                        let vao = vao.lock().unwrap();
                        let ebo = ebo.lock().unwrap();
                        let prog = prog.lock().unwrap();

                        gl::BindVertexArray(*vao);
                        gl::BindBuffer(gl::ARRAY_BUFFER, *vbo);
                        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, *ebo);
                        gl::UseProgram(*prog);
                        gl::EnableVertexAttribArray(0);
                        gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE, 0, 0 as *mut c_void);
                        gl::DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_SHORT, 0 as *mut c_void);
                        gl::DisableVertexAttribArray(0);

                        gl::UseProgram(0);
                        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
                        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
                        gl::BindVertexArray(0);
                    }
                    // THIS WILL CAUSE THE APPLICATION TO CRASH IF IT IS MINIMIZED FOR TOO LONG
                    // ON WINDOWS AT LEAST
                    gl_area.queue_render();
                    // SO JUST DON'T MINIMIZE IT
                    Inhibit(false)
                });
        }

        canvas.set_resize_fn(|_gl, width, height| {
                unsafe {
                    // let width_location =
                    //     gl::GetUniformLocation(program, CString::new("width").unwrap().as_ptr());
                    // let height_location =
                        //     gl::GetUniformLocation(program, CString::new("height").unwrap().as_ptr());

                    gl::Viewport(0, 0, width, height);
                    // gl::Uniform1i(width_location, width);
                    // gl::Uniform1i(height_location, height);
                }
            });

        draw_area.add(canvas.get_gtk_gl());

        reload_thm.connect_activate(|_| {
                Theme::update_theme(&Variant::Darker);
            });

        window.maximize();
        window.set_title("Table v0.1.4");
        window.set_wmclass("Table", "Table");
        window.set_icon_from_file(Path::new("res/img/ico/icon.ico")).unwrap_or_else(|_| {
                println!("Failed to load icon");
            });
        window.connect_delete_event(|main, _| {
                main.destroy();
                super::gtk::main_quit();
                Inhibit(false)
            });
        window.show_all();
    }

    pub fn get_object<T: IsA<Object>>(&self, object: &str) -> Option<T> {
        self.builder.clone().get_object(object)
    }
}
